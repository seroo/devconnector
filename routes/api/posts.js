//yazılanlar için

const express = require("express");

const router = express.Router();

// burasi aslinda ./api/users/test anlamina geliyor
// @route GET api/posts/test
// desc Tests post route
// @access Public
router.get("/test", (req, res) => res.json({
    msg: "Posts Works"
}));

module.exports = router;