// kişi bilgileri için kullanılacak

const express = require("express");

const router = express.Router();

//burasi aslinda ./api/users/test anlamina geliyor
// @route GET api/profile/test
// desc Tests profile route
// @access Public
router.get("/test", (req, res) => res.json({
    msg: "Profile Works"
}));

module.exports = router;